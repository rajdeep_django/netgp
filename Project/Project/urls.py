from django.contrib import admin
from django.urls import path, include   #new 
from django.conf.urls.static import static  #new
from django.conf import settings    #new

urlpatterns = [
    path('admin/', admin.site.urls),
    path('app/', include('app.urls')),    #new
    path('ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)    #new
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)    #new
