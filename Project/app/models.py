from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField

class Student(models.Model):
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    email = models.EmailField()
    password = models.CharField(max_length=20)
    phn_no = models.CharField(max_length=15, blank=True, null=True)
    dob = models.DateField(auto_now_add=False)
    bio = models.TextField()
    age = models.IntegerField(default=50)
    registration_time = models.TimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.first_name

# One To One
class UserDetail(models.Model): 
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True
    )
    dob = models.DateField(auto_now_add=False)
    phn_no = models.CharField(max_length=15)
    gender = models.CharField(max_length=10)
    profile_image = models.ImageField(upload_to='Profile_Image/')   #pip install pillow
    
    def __str__(self):
        return self.user.username

class Post(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="User_Post",
    )
    title = models.CharField(max_length=20)
    desc = models.TextField()
    image = models.ImageField(upload_to='Post/')
    created_at = models.TimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username

#ForeignKey
# class Batch(models.Model):
#     batch_id = models.CharField(max_length=10)
#     batch_name = models.CharField(max_length=50)

#     def __str__(self):
#         return self.batch_name
    

# class Student_2(models.Model):
#     name = models.CharField(max_length=100)
#     email = models.EmailField()
#     batch = models.ForeignKey(
#             Batch,
#             on_delete=models.CASCADE,
#             related_name="batch_allocating",
#             blank=True,
#             null=True
#         )


# Many To Many
class Course(models.Model):
    name = models.TextField()
    year = models.IntegerField()
  
    def __str__(self):
        return self.name
  

class Person(models.Model):
    last_name = models.TextField()
    first_name = models.TextField()
    courses = models.ManyToManyField("Course")

class CMS_Model(models.Model):
    tc = RichTextUploadingField()