from django.contrib import admin
from django.contrib.auth.models import User, Group
from .models import *

class view_student(admin.ModelAdmin):
    list_display =['first_name', 'last_name', 'email', 'phn_no', 'age']

admin.site.register(Student, view_student)
admin.site.register(UserDetail)
#admin.site.unregister(User)
admin.site.unregister(Group)
# admin.site.register(Batch)
# admin.site.register(Student_2)
admin.site.register(Course)
admin.site.register(Person)
admin.site.register(Post)
admin.site.register(CMS_Model)
