from django.urls import path
from .views import *

urlpatterns = [
    path('test/', test_view, name="test_url"),
    path('test2/', test_view_2, name="test2_url"),
    path('test3/', test_view_3, name="test3_url"),
    path('demo/', demo_view, name="demo_url"),
    path('login/', login_view, name="login_name"),
    path('home/', homepage_view, name="home_url"),
    path('logout/', logout_view, name="logout_name"),
    path('create-post/', create_post_view, name="create_post_url"),
    path('regi/', registration_view, name="regi_url"),
    path('show-data/', show_my_data, name='show_data_url'),
    path('delete_post/<int:post_id>', delete_post, name='delete_post_url'),
    path('form/', django_form, name='form_url'),
    path('terms/', terms_view, name='terms_url'),
]