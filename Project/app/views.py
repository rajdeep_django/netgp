from turtle import title
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from .models import *
from .forms import *

def test_view(request):
    return HttpResponse('This is Test View !!!')

def test_view_2(request):
    return HttpResponse('This is Test View 2 !!!')

def test_view_3(request):
    return HttpResponse('This is Test View 3 !!!')

def demo_view(request):
    name = "Ajit Kumar"
    return render(request, 'app/demo.html', context={'data' : name})

def login_view(request):
    try:
        request.session['u.username']
        return redirect("/app/home/")
    except:
        pass
    if request.method == "GET":
        return render(request, 'app/login.html', context={})
    else:
        username = request.POST['username']
        password = request.POST['pass']

        try:
            u = User.objects.get(username=username)
            if check_password(password, u.password):
                request.session['u.username'] = username
                return redirect("/app/home/")
            else:
                return redirect("/app/login/")
        except Exception as e:
            print("#"*10, e)
            return redirect("/app/login/")

def registration_view(request):
    try:
        request.session['u.username']
        return redirect("/app/home/")
    except:
        pass
    if request.method == "GET":
        return render(request, "app/registration.html", context={})
    else:
        #print(request.POST)
        f_name = request.POST['fn']
        l_name = request.POST['ln']
        email = request.POST['email']
        username = request.POST['username']
        phn_no = request.POST['phn']
        gender = request.POST['gender']
        dob = request.POST['birthday']
        password = request.POST['pass']
        profile_pic = request.FILES['img']
        #print("First Name : ", f_name)

        try:
            u = User(first_name=f_name, last_name=l_name, email=email, username=username)
            u.save()
            u.set_password(password)
            u.save()

            ud = UserDetail(user=u, dob=dob, phn_no=phn_no, gender=gender, profile_image=profile_pic)
            ud.save()
            return redirect("/app/login/")
        except:
            return redirect('/app/regi/')

def show_my_data(request):
    try:
        request.session['u.username']
        s = Student.objects.all().filter(age=30)
        return render(request, 'app/show-my-data.html', context={'data' : s})
    except:
        return redirect("/app/login/")

def homepage_view(request):
    try:
        u = request.session['u.username']
        ud = UserDetail.objects.get(user__username=u)
        p = Post.objects.all().order_by('-created_at')
        return render(request, 'app/homepage.html', context={'post' : p, "ud" : ud})
    except:
        return redirect("/app/login/")

def create_post_view(request):
    try:
        u = request.session['u.username']
        if request.method == "GET":
            return render(request, 'app/create-post.html', context={})
        else:
            user = User.objects.get(username=u)
            title = request.POST['headername']
            body = request.POST['message']
            image = request.FILES['imgfile']
            
            p = Post(user=user, title=title, desc=body, image=image)
            p.save()

            return redirect("/app/home/")
    except:
        return redirect("/app/login/")

def logout_view(request):
    try:
        del request.session['u.username']
    except:
        pass
    return redirect("/app/login/")

def delete_post(request, post_id):
    try:
        request.session['u.username']
        p = Post.objects.get(id=post_id)
        p.delete()
        return redirect("/app/home/")
    except:
        return redirect("/app/login/")


def django_form(request):
    # form = Login_Form()
    # form = Student_form()
    form = User_registration_form()
    return render(request, 'app/django_form.html', context={'form' : form})

def terms_view(request):
    cms = CMS_Model.objects.get(id=1)
    return render(request, 'app/terms.html', context={'cms' : cms})