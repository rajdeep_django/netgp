from .models import *
from django import forms
from django.contrib.auth.models import User

# Normal Form

BIRTH_YEAR_CHOICES = ['1980', '1981', '1982']
FAVORITE_COLORS_CHOICES = [
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
]

class Login_Form(forms.Form):
    username = forms.CharField(max_length=10, min_length=4, help_text="* required", label="Enter your username ")
    password = forms.CharField(max_length=10, min_length=4, widget=forms.PasswordInput())
    birth_year = forms.DateField(widget=forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES))
    favorite_colors = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        choices=FAVORITE_COLORS_CHOICES,
    )


# Model Form
class Student_form(forms.ModelForm):
    class Meta:
        model = Student
        # fields = "__all__"
        fields = ['first_name', 'last_name']


class User_registration_form(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"

