# def m1():
#     for i in range(1, 21):
#         print("m1 : ", i)

# def m2():
#     for i in range(100, 151):
#         print("m2 : ", i)

# m1()
# m2()
# print("Done...")


# import threading

# def m1():
#     for i in range(1, 21):
#         print("m1 : ", i)

# def m2():
#     for i in range(100, 151):
#         print("m2 : ", i)

# t1 = threading.Thread(target=m1)
# t2 = threading.Thread(target=m2)

# t1.start()
# t2.start()
# print("Done...")


# import time

# for i in range(1, 6):
#     print(i)
#     time.sleep(5)